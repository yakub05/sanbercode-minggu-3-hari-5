@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection
@section('isi')

        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Cast</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

            </div>
            <div class="form-group">
                <label for="title">Umur Cast</label>
                <input type="text" class="form-control" name="umur" id="title" placeholder="Masukkan Title">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Bio Cast</label>
                <input type="text" class="form-control" name="bio" id="title" placeholder="Masukkan Title">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                
            <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </form>
@endsection